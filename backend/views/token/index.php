<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Токены';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="token-index">

    <p>
        <?php echo Html::a(
            'Сгенерировать токен',
            ['create'],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'val',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}'
            ]
        ]
    ]); ?>

</div>
