<?php

use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="email-template-form">

    <?php $form = ActiveForm::begin(['id' => 'email-template-form', 'options'=>['enctype'=>'multipart/form-data']]); ?>
    
    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'file_name')->hiddenInput(['id' => 'file-name-input'])->label(false) ?>
    
    <div class="form-group">
        <label class="control-label">Имя файла</label>: 
        <span id="file-name"><?=($model->file_name)?$model->file_name:'файл не выбран';?></span>
    </div>
    
    <?php echo $form->field($model, 'html')->textarea() ?>
    
    <div class="form-group">
        <label class="control-label">Список изображений:</label>
        <span id="img-list"><?=($model->imgs)?implode(',', $model->imgs):'нету';?></span>
    </div>
    
    <?php echo $form->field($model, 'text')->textarea() ?>
    
    <?php echo $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'from_name')->textInput(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'from_email')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'reply_to')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label class="control-label">Список параметров:</label>
        <div class="params-list">
            <?php foreach ($params as $param): ?>
                <div class="param-item">
                    <label>Название</label>: <input type="text" name="param_name[]" value="<?=$param['name'];?>"> 
                    <label>По-умолчанию</label>: <input type="text" name="param_val[]" value="<?=$param['val'];?>"> 
                    <a class="delete-param"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            <?php endforeach;?>
        </div>
        <a id="add-param" class="btn btn-primary">Создать параметр</a>
    </div>
    
    <div class="form-group">
        <?=yii\helpers\Html::fileInput('template-file', '', ['id' => 'template-file']);?>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="param-el-template hide">
        <div class="param-item">
            <label>Название</label>: <input type="text" name="param_name[]" value=""> 
            <label>По-умолчанию</label>: <input type="text" name="param_val[]" value=""> 
            <a class="delete-param"><span class="glyphicon glyphicon-trash"></span></a>
        </div>
    </div>
</div>

<?php $this->registerJs('emailTemplate.init();');?>
