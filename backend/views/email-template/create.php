<?php
/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */

$this->title ='Создание  шаблона';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'params' => $params
    ]) ?>

</div>
