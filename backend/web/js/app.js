$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
    
})

var emailTemplate = function (){
    function init() {
        $('#add-param').on('click', addParam);
        $('.params-list').on('click', '.delete-param', deleteParam);
        $('#template-file').on("click", function(){return 1;confirm("Вы уверены? Текст поля Html будет заменен!");});
        $('#template-file').on("change", uploadFile);
    }
    function addParam(e) {
        e.preventDefault();
        $('.params-list').append('<div>' + $('.param-el-template').html() + '</div>');
    }
    function deleteParam(e) {
        e.preventDefault();
        $(this).closest('.param-item').remove();
    }
    function uploadFile() {
        var file_data = $('#template-file').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        $.ajax({
            url : 'upload-file',
            type : 'POST',
            data : form_data,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(data) {
                if(data.result == 'success'){
                    $('#emailtemplate-html').val(data.html);
                    $('#file-name').html(data.file_name);
                    $('#file-name-input').val(data.file_name);
                    $('#img-list').html(data.imgs);
                    alert('Успех');
                    return true;
                }
                else if(data.result == 'error'){
                    alert(data.message);
                    return true;
                }
                alert('Что-то пошло не так (');
            },
            done: function(){
                alert('Что-то пошло не так (');
            }
        });
    }

    return {
        init: function() {
            init();
        }
    };
}();