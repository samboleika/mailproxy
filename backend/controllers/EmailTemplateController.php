<?php

namespace backend\controllers;

use Yii;
use common\models\EmailTemplate;
use backend\models\search\EmailTemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmailTemplateController implements the CRUD actions for EmailTemplate model.
 */
class EmailTemplateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post']
                ]
            ]
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder'=>['created_at'=>SORT_DESC]
        ];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new EmailTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailTemplate();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $this->validateParams($model) && $model->save() && $this->saveParams($model->id)) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'params' => $this->getPostParams()
            ]);
        }
    }

    /**
     * Updates an existing EmailTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $this->validateParams($model) && $model->save() && $this->saveParams($model->id)) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'params' => ($this->getPostParams())?$this->getPostParams():$model->emailTemplateParam
            ]);
        }
    }

    /**
     * Deletes an existing EmailTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionUploadFile()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $file_upload = \yii\web\UploadedFile::getInstanceByName('file');
        
        if(!in_array($file_upload->extension, ['html', 'htm'])){
            return ['result' => 'error', 'message' => 'Неверный формат файла. Допустимые форматы: html, htm'];
        }
        
        $handle = fopen($file_upload->tempName, 'r');
        $contents = fread($handle, filesize($file_upload->tempName));
        $name = $file_upload->name;
        fclose($handle);
        
        if(!json_encode($contents)){
            return ['result' => 'error', 'message' => 'Неверная кодировка файла. Допустимая кодировка: utf-8'];
        }
        
        $imgs = $this->uploadImgs($contents);
        if(count($imgs['imgs_error'])){
            return ['result' => 'error', 'message' => 'Не получается получить некоторые изображения: '. implode(', ', $imgs['imgs_error'])];
        }
        
        return ['result' => 'success', 'html' => $imgs['content'], 'file_name' => $name, 'imgs' => implode(', ', $imgs['imgs'])];
    }

    /**
     * Finds the EmailTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function uploadImgs($file)
    {
        $imgs = [];
        $imgs_uploaded = [];
        $imgs_error = [];
        
        preg_match_all('/(https|http):\/\/.+\.(jpg|jpeg|png|bmp|gif)/i', $file, $imgs);
        
        foreach ($imgs[0] as $key => $img) {
            $content = @file_get_contents($img);
            if($content){
                $img_name = DIRECTORY_SEPARATOR.EmailTemplate::IMG_PATH.DIRECTORY_SEPARATOR.time().'_'.$key.'.'.pathinfo($img, PATHINFO_EXTENSION);
                $imgs_uploaded[] = \Yii::$app->request->hostInfo . $img_name;
                file_put_contents(\Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . $img_name , $content);
                $file = str_replace($img, \Yii::$app->request->hostInfo . $img_name, $file);
            }
            else{
                $imgs_error[] = $img;
            }
        }
        
        return ['imgs' => $imgs_uploaded, 'imgs_error' => $imgs_error, 'content' => $file];
    }
    
    protected function validateParams($model)
    {
        $params = Yii::$app->request->post('param_name');
        if(empty($params)) return true;
        
        foreach ($params as $value) {
            if(!preg_match('/{{('.$value.')}}/i', $model->html) || !preg_match('/{{('.$value.')}}/i', $model->text)){
                Yii::$app->session->setFlash('alert', [
                    'options'=>['class'=>'alert-danger'],
                    'body'=>'Некоторые параметры отсутствуют в html или text письма:' . $value
                ]);
                return false;
            }
        }
        
        return true;
    }
    
    protected function getPostParams()
    {
        $params_name = Yii::$app->request->post('param_name');
        $params_val = Yii::$app->request->post('param_val');
        $params = [];
        
        if(empty($params_name)) return $params;
        
        foreach ($params_name as $key => $value) {
            $params[] = ['name' => $value, 'val' => $params_val[$key]];
        }
        
        return $params;
    }
    
    protected function saveParams($template_id)
    {
        $params = $this->getPostParams();
        
        \common\models\EmailTemplateParam::deleteAll(['template_id' => $template_id]);
        
        foreach ($params as $param) {
            $emailTemplateParam = new \common\models\EmailTemplateParam();
            $emailTemplateParam->template_id = $template_id;
            $emailTemplateParam->name = $param['name'];
            $emailTemplateParam->val = $param['val'];
            if(!$emailTemplateParam->save()) return false;
        }
        
        return true;
    }
}
