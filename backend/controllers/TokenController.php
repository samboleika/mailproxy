<?php

namespace backend\controllers;

use Yii;
use common\models\Token;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TokenController implements the CRUD actions for Token model.
 */
class TokenController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post']
                ]
            ]
        ];
    }

    /**
     * Lists all Token models.
     * @return mixed
     */
    public function actionIndex()
    { 
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Token::find(),
        ]);
        $dataProvider->sort = [
            'defaultOrder'=>['created_at'=>SORT_DESC]
        ];
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /**
     * Creates a new Token model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Token();
        $model->val = \Yii::$app->security->generateRandomString(32);
        $model->user_id = \Yii::$app->user->id;
        if(!$model->save()){
            Yii::$app->session->setFlash('alert', [
                'options'=>['class'=>'alert-danger'],
                'body'=>'Не получилось создать токен: ' . var_export($model->getErrors(), true)
            ]);
        }
        return $this->redirect(['index']);
    }
    
    /**
     * Deletes an existing Token model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Token model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Token the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Token::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
