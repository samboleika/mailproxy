<?php
namespace frontend\modules\api\v1\actions\emails;

use yii\base\Action;
use yii\web\BadRequestHttpException;
use Yii;
use common\models\EmailTemplate;
use common\models\Message;

class CreateAction extends Action
{
    public $template;
    /**
     * @return mixed|static
     */
    public function run()
    {
        $data = json_decode(\Yii::$app->request->post('email'), true);
        $this->validateData($data);
        $params = $this->mergeParams($data['parameters']);
        $unsubscribe_hash = \Yii::$app->security->generateRandomString(32);
        $send_data = [
            'html' => $this->template->readyHtml($params, $unsubscribe_hash),
            'text' => $this->template->readyText($params, $unsubscribe_hash),
            'subject' => $this->template->subject,
            'from' => [ 'name' => $this->template->from_name, 'email' => $this->template->from_email ],
            'to' => $data['to'],
            'bbc' => $data['bbc']
        ];
        //print_r($send_data);exit;
        $result = Yii::$app->sendpulse->smtpSendMail($send_data);
        if($result->result){
            $persond_id = (isset($data['person_id']))?$data['person_id']:0;
            $this->saveMessage($params, $data['to']['email'], $persond_id, $unsubscribe_hash);
        }
        return $result;
    }
    
    protected function validateData($data){
        if(isset($data['template_id']) && isset($data['person_id']) && isset($data['to'])){
            $this->template = EmailTemplate::findOne((int)$data['template_id']);
            if(!empty($this->template) && (!isset($data['parameters']) || isset($data['parameters']) && is_array($data['parameters']))){
                return true;
            }
        }
        throw new BadRequestHttpException('Invalid JSON');
    }
    
    protected function mergeParams($params){
        $result_params = [];
        $default_params = $this->template->emailTemplateParam;
        if(!empty($default_params)){
            foreach($default_params as $default_param){
                $result_params[$default_param->name] = (isset($params[$default_param->name]))?(string)$params[$default_param->name]:$default_param->val;
            }
        }
        
        return $result_params;
    }
    
    protected function saveMessage($params, $email, $person_id, $unsubscribe_hash) {
        $request = \Yii::$app->request;
        
        $last_message = Yii::$app->sendpulse->smtpListEmails(1);
        
        $message = new Message();
        $message->sendpulse_email_id = (!empty($last_message[0]->id))?$last_message[0]->id:0;
        $message->template_id = $this->template->id;
        $message->params = \json_encode($params);
        $message->email = $email;
        $message->person_id = $person_id;
        $message->unsubscribe_status = 0;
        $message->unsubscribe_hash = $unsubscribe_hash;
        $message->save();
    }
}
