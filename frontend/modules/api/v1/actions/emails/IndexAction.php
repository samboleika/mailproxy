<?php
namespace frontend\modules\api\v1\actions\emails;

use yii\base\Action;
use yii\base\InvalidParamException;
use Yii;
use common\models\Message;

    /**
     * class IndexAction get all sended messages
     */

class IndexAction extends Action
{
    /**
     * @return array
     */
    public function run()
    {
        $request = \Yii::$app->request;
        $result = Yii::$app->sendpulse->smtpListEmails($request->get('limit'), $request->get('offset'), $request->get('from'), $request->get('to'), $request->get('sender'), $request->get('recipient'));
        if(empty($result->http_code)){
            foreach($result as $key => $email_info) {
                unset($email_info->sender_ip);
                $message = Message::find()->where(['sendpulse_email_id' => $email_info->id])->one();
                if(!empty($message)){
                    $email_info->person_id = $message->person_id;
                    $email_info->unsubscribe = $message->unsubscribe_status;
                }
                $result[$key] = $email_info;
            }
        }
        return $result;
    }
}
