<?php
namespace frontend\modules\api\v1\actions\emails;

use yii\base\Action;
use yii\base\InvalidParamException;
use Yii;
use common\models\Message;

    /**
     * class IndexAction get sended message
     */

class ViewAction extends Action
{
    /**
     * Displays a email message.
     * @param string $id the primary key of the email message.
     * @return array
     */
    public function run($id)
    {
        $result = Yii::$app->sendpulse->smtpGetEmailInfoById($id);
        if(empty($result->http_code)){
            unset($result->sender_ip);
            $message = Message::find()->where(['sendpulse_email_id' => $result->id])->one();
            if(!empty($message)){
                $result->person_id = $message->person_id;
                $result->unsubscribe = $message->unsubscribe_status;
            }
        }
        return $result;
    }
}
