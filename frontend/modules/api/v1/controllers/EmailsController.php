<?php
namespace frontend\modules\api\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
/**
 * Class EmailsController
 * @author Eugene Terentev <eugene@terentev.net>
 */
class EmailsController extends Controller
{ 
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['index', 'view', 'create']
            ],
        ];
    } 
    public function init()
    {
        parent::init();
        $handler = new \frontend\modules\api\v1\components\ApiErrorHandler;
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
    
    public function beforeAction($action) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->checkAccess();
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        if(!empty($result->http_code)){
            Yii::$app->response->setStatusCode($result->http_code);
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'frontend\modules\api\v1\actions\emails\IndexAction'
            ],
            'create' => [
                'class' => 'frontend\modules\api\v1\actions\emails\CreateAction'
            ],
            'view' => [
                'class' => 'frontend\modules\api\v1\actions\emails\ViewAction'
            ]
        ];
    }
    
    private function checkAccess() {
        $token = \common\models\Token::find()->where(['val' => Yii::$app->request->get('access_token')])->one();
        if(empty($token)){
            throw new \yii\web\UnauthorizedHttpException('Invalid token');
        }
    }
}
