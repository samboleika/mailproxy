<?php
namespace frontend\modules\api\v1\components;

use Yii;
use stdClass;
use yii\base\Exception;

class SendPulse extends \yii\base\Component
{
    private $apiUrl = 'https://api.sendpulse.com';

    public $userId;
    public $secret;
    private $token = NULL;

    private $refreshToken = 0;

    /*
     *  Define where script will save access token
     *  Types: session, file, memcache
     */
    public $storageType = '';

    private $apiFilesPath = '';


    public function init()
    {
        if (empty($this->userId) || empty($this->secret)) {
            throw new Exception('Empty ID or SECRET');
        }
        
        $hashName = md5($this->userId . '::' . $this->secret);

        switch ($this->storageType) {
            case 'session':
                if (isset($_SESSION[$hashName]) && !empty($_SESSION[$hashName])) {
                    $this->token = $_SESSION[$hashName];
                }
                break;
            case 'memcache':
                $memcache = new Memcache();
                $memcache->connect('localhost', 11211) or die('Could not connect to Memcache');
                $token = $memcache->get($hashName);
                if (!empty($token)) {
                    $this->token = $token;
                }
                break;
            default:
                $filePath = $this->apiFilesPath . $hashName;
                if (file_exists($filePath)) {
                    $this->token = file_get_contents($filePath);
                }
        }

        if (empty($this->token)) {
            if (!$this->getToken()) {
                throw new Exception('Could not connect to api, check your ID and SECRET');
            }
        }

        parent::init(); // TODO: Change the autogenerated stub
    }
    
    /**
     * Get token and store it
     *
     * @return bool
     */
    private function getToken()
    {
        $data = array(
            'grant_type' => 'client_credentials',
            'client_id' => $this->userId,
            'client_secret' => $this->secret,
        );

        $requestResult = $this->mySendRequest('oauth/access_token', 'POST', $data, false);

        if ($requestResult->http_code != 200) {
            return false;
        }

        $this->refreshToken = 0;
        $this->token = $requestResult->data->access_token;

        $hashName = md5($this->userId . '::' . $this->secret);
        switch ($this->storageType) {
            case 'session':
                $_SESSION[$hashName] = $this->token;
                break;
            case 'memcache':
                $memcache = new \Memcache();
                $memcache->connect('localhost', 11211) or die('Could not connect to Memcache');
                $memcache->set($hashName, $this->token, false, 3600);
                break;
            default:
                $tokenFile = fopen($this->apiFilesPath . $hashName, "w");
                fwrite($tokenFile, $this->token);
                fclose($tokenFile);
        }

        return true;
    }

    /**
     * Form and send request to API service
     *
     * @param $path
     * @param string $method
     * @param array $data
     * @param bool $useToken
     * @return array|NULL
     */
    private function mySendRequest($path, $method = 'GET', $data = array(), $useToken = true)
    {
        $url = $this->apiUrl . '/' . $path;
        $method = strtoupper($method);
        $curl = curl_init();

        if ($useToken && !empty($this->token)) {
            $headers = array('Authorization: Bearer ' . $this->token);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, count($data));
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            default:
                if (!empty($data)) {
                    $url .= '?' . http_build_query($data);
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($curl);
        $header_info = curl_getinfo($curl,CURLINFO_HEADER_OUT);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $headerCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $responseBody = substr($response, $header_size);

        curl_close($curl);
        
        Yii::info(['header_get' => \Yii::$app->request->absoluteUrl, 'body_get' => \Yii::$app->request->rawBody, 'header_send' => $header_info, 'body_send' => $data], 'apiRequest');
        
        if ($headerCode == 401 && $this->refreshToken == 0) {
            $this->refreshToken += 1;
            $this->getToken();
            $return = $this->mySendRequest($path, $method, $data);
        } else {
            $return = new stdClass();
            $return->data = json_decode($responseBody);
            $return->http_code = $headerCode;
        }

        return $return;
    }

    /**
     * Process results
     *
     * @param $data
     * @return mixed
     */
    private function handleResult($data)
    {
        if (empty($data->data)) {
            $data->data = new stdClass();
        }
        if ($data->http_code != 200) {
            $data->data->is_error = true;
            $data->data->http_code = $data->http_code;
        }

        return $data->data;
    }

    /**
     * Process errors
     *
     * @param null $customMessage
     * @return stdClass
     */
    private function handleError($customMessage = NULL)
    {
        $message = new stdClass();
        $message->is_error = true;
        if (!is_null($customMessage)) {
            $message->message = $customMessage;
        }

        return $message;
    }

    /**
     * SMTP: get list of emails
     *
     * @param int $limit
     * @param int $offset
     * @param string $fromDate
     * @param string $toDate
     * @param string $sender
     * @param string $recipient
     * @return mixed
     */
    public function smtpListEmails($limit = 0, $offset = 0, $fromDate = '', $toDate = '', $sender = '', $recipient = '')
    {
        $data = array(
            'limit' => $limit,
            'offset' => $offset,
            'from' => $fromDate,
            'to' => $toDate,
            'sender' => $sender,
            'recipient' => $recipient
        );

        $requestResult = $this->mySendRequest('/smtp/emails', 'GET', $data);

        return $this->handleResult($requestResult);
    }

    /**
     * Get information about email by id
     *
     * @param $id
     * @return mixed|stdClass
     */
    public function smtpGetEmailInfoById($id)
    {
        if (empty($id)) {
            return $this->handleError('Empty id');
        }

        $requestResult = $this->mySendRequest('/smtp/emails/' . $id);

        return $this->handleResult($requestResult);
    }

    /**
     * SMTP: send mail
     *
     * @param $email
     * @return mixed|stdClass
     */
    public function smtpSendMail($email)
    {
        if (empty($email)) {
            return $this->handleError('Empty email data');
        }

        $email['html'] = base64_encode($email['html']);
        $data = array(
            'email' => serialize($email)
        );

        $requestResult = $this->mySendRequest('smtp/emails', 'POST', $data);

        return $this->handleResult($requestResult);
    }
}