<?php
return [
    'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    'rules'=> [
        // Api mail proxy
        [
            'class' => 'yii\rest\UrlRule', 
            'controller' => 'api/v1/emails', 
            'only' => ['create', 'index', 'view'],
            'tokens' => [
                '{id}' => '<id:\\w+>'
            ]
        ],
    ]
];
