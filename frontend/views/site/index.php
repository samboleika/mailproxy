<?php
/* @var $this yii\web\View */
$this->title = 'Mail Api';
?>
<div class="site-index">
    <legend><h2>Доступные методы</h2></legend>
    <h4>Отправить письмо</h4>
    <p>Для того, чтобы отправить письмо, необходимо отправить POST запрос по ссылке /api/v1/emails?access_token=TOKEN</p>

    Параметры запроса:<br/>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <tr>
            <td><code>email</code></td>
            <td>сериализованный массив с данными письма</td>
        </tr>
    </table>

    Параметры массива email:<br/>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <tr>
            <td><code>template_id</code></td>
            <td>id шаблона письма/td>
        </tr>
        <tr>
            <td><code>person_id</code></td>
            <td>ID потребителя в базе ФМ</td>
        </tr>
        <tr>
            <td><code>parameters</code></td>
            <td>Параметры шаблона<br>
                <code>"parameters": {
                "parameter1": "parameter1 value",
                "parameter1": "parameter2 value"
                }</code>
          </td>
        </tr>
        <tr>
            <td><code>to</code></td>
            <td>
                массив с именем и адресом отправителя<br>
                <code>"to": {
                    "name": "Recipient1",
                    "email": "recipient1@mail.com"
                }</code>
            </td>
        </tr>
        <tr>
            <td><code>bcc</code></td>
            <td>массив получателей<br/>
                <code>
                "bcc" : [
                    {
                    "name" : "Recipient1 name",
                    "email" : "recipient1@example.com"
                    },
                    {
                    "name" : "Recipient2 name",
                    "email" : "recipient2@example.com"
                    },
                ]
                </code>
            </td>
        </tr>
    </table>

    <p>В случае успеха сервер вернет JSON строку с <samp>"result": true</samp></p>

    <h4>Получить список писем</h4>
    <p>Для получения списка всех отправленных писем, нужно сделать GET запрос по ссылке /api/v1/emails?access_token=TOKEN</p>
    Параметры запроса (необязательные):<br/>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <tr>
            <td><code>limit</code></td>
            <td>количество записей</td>
        </tr>
        <tr>
            <td><code>offset</code></td>
            <td>смещение для выборки</td>
        </tr>
        <tr>
            <td><code>from</code></td>
            <td>начальная дата для выборки</td>
        </tr>
        <tr>
            <td><code>to</code></td>
            <td>максимальная дата для выборки</td>
        </tr>
        <tr>
            <td><code>sender</code></td>
            <td>отправитель</td>
        </tr>
        <tr>
            <td><code>recipient</code></td>
            <td>получатель</td>
        </tr>
    </table>
    <h4>Получить информацию по конкретному письму</h4>
    <p>Для получения информации по конкретному письму, нужно сделать GET запрос по ссылке /api/v1/emails/{id}?access_token=TOKEN</p>

    Параметры запроса:<br/>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <tr>
            <td><code>id</code></td>
            <td>идентификатор письма</td>
        </tr>
    </table>
</div>
