<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionUnsubscribe()
    {
        $hash = Yii::$app->request->get('hash');
        $message = \common\models\Message::find()->where(['unsubscribe_hash' => $hash])->one();
        if(!empty($message)){
            $message->unsubscribe_status = 1;
            $message->save();
            return "Вы успешно отписаны";
        }
        
        return "Такого email не найдено";
    }
}
