<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $sendpulse_email_id
 * @property integer $template_id
 * @property string $params
 * @property string $email
 * @property integer $person_id
 * @property string $unsubscribe_hash
 * @property integer $unsubscribe_status
 * @property integer $created_at
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'person_id', 'unsubscribe_status', 'created_at'], 'integer'],
            [['sendpulse_email_id'], 'string', 'max' => 255],
            [['params'], 'string', 'max' => 1000],
            [['email', 'unsubscribe_hash'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sendpulse_email_id' => 'Sendpulse Email ID',
            'template_id' => 'Template ID',
            'params' => 'Params',
            'email' => 'Email',
            'person_id' => 'Person ID',
            'unsubscribe_hash' => 'Unsubscribe Hash',
            'unsubscribe_status' => 'Unsubscribe Status',
            'created_at' => 'Created At',
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}
