<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_template_param".
 *
 * @property integer $id
 * @property integer $template_id
 * @property string $name
 * @property string $val
 *
 * @property EmailTemplate $template
 */
class EmailTemplateParam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_template_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'name', 'val'], 'required'],
            [['template_id'], 'integer'],
            [['name', 'val'], 'string', 'max' => 255],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_id' => 'Template ID',
            'name' => 'Name',
            'val' => 'Val',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(EmailTemplate::className(), ['id' => 'template_id']);
    }
}
