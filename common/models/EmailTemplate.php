<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $html
 * @property string $text
 * @property string $subject
 * @property string $from_name
 * @property string $from_email
 * @property string $reply_to
 * @property integer $created_at
 *
 * @property EmailTemplateParam[] $emailTemplatesParam
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    CONST IMG_PATH = 'template-imgs';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['html', 'text', 'subject', 'from_name', 'reply_to'], 'required'],
            [['html', 'text'], 'string'],
            [['created_at'], 'integer'],
            [['name', 'file_name', 'subject', 'from_name', 'from_email', 'reply_to'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'file_name' => 'Файл',
            'html' => 'Html',
            'text' => 'Text',
            'subject' => 'Тема',
            'from_name' => 'Имя отправителя',
            'from_email' => 'Email отправителя',
            'reply_to' => 'Reply To',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailTemplateParam()
    {
        return $this->hasMany(EmailTemplateParam::className(), ['template_id' => 'id']);
    }
    
    /**
     * @return array
     */
    public function getImgs()
    {
        $imgs = [];
        preg_match_all('/(https|http):\/\/.+\.(jpg|jpeg|png|bmp|gif)/i', $this->html, $imgs);
        return $imgs[0];
    }
    
    /**
     * @return array
     */
    static function replaceParams($params, $string) {
        $patterns = [];
        $replacements = [];
        foreach ($params as $name => $val) {
            $patterns[] = '/{{' . $name . '}}/';
            $replacements[] = $val;
        }
        return preg_replace($patterns, $replacements, $string);
    }
    
    public function addUnsubscribeLinkHtml($html, $unsubscribe_hash) {
        $html .= '<p style="text-align: center"><a href="'.\Yii::$app->request->hostInfo.'/site/unsubscribe?hash='.$unsubscribe_hash.'">Отписаться от рассылок</a></p>';
        return $html;
    }
    
    public function addUnsubscribeLinkText($text, $unsubscribe_hash) {
        $text .= ' Отписаться от рассылок - '.\Yii::$app->request->hostInfo.'/site/unsubscribe?hash='.$unsubscribe_hash;
        return $text;
    }
    
    public function readyHtml($param, $unsubscribe_hash) {
        $result = self::replaceParams($param, $this->html);
        return $this->addUnsubscribeLinkHtml($result, $unsubscribe_hash);
    }
    
    public function readyText($param, $unsubscribe_hash) {
        $result = self::replaceParams($param, $this->text);
        return $this->addUnsubscribeLinkText($result, $unsubscribe_hash);
    }
}
