<?php

namespace common\components\log;

use Yii;

class MyFileTarget extends \yii\log\FileTarget
{    
    /**
     * Formats a log message for display as a string.
     * @param array $message the log message to be formatted.
     * The message structure follows that in [[Logger::messages]].
     * @return string the formatted message
     */
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;
        if (!is_string($text)) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string) $text;
            } else {
                $text = \yii\helpers\VarDumper::export($text);
            }
        }
        
        return date('Y-m-d H:i:s', $timestamp) . ' ' . Yii::$app->request->userIP . ' ' . $text;
    }
}
