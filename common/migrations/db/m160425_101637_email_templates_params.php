<?php

use yii\db\Migration;

class m160425_101637_email_templates_params extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%email_template_param}}', [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'val' => $this->string(255)->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey('fk_email_template', '{{%email_template_param}}', 'template_id', '{{%email_template}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fk_email_template', '{{%email_template_param}}');
        $this->dropTable('{{%email_template_param}}');
    }
}
