<?php

use yii\db\Migration;

class m160509_172749_messages extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'sendpulse_email_id' => $this->string(255),
            'template_id' => $this->integer(),
            'params' => $this->string(1000),
            'email' => $this->string(150),
            'person_id' => $this->integer(),
            'unsubscribe_hash' => $this->string(150),
            'unsubscribe_status' => $this->integer(1),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%message}}');
    }
}
