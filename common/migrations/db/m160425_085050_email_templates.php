<?php

use yii\db\Migration;

class m160425_085050_email_templates extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%email_template}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150),
            'file_name' => $this->string(150),
            'html' => $this->text()->notNull(),
            'text' => $this->text()->notNull(),
            'subject' => $this->string(150)->notNull(),
            'from_name' => $this->string(150)->notNull(),
            'from_email' => $this->string(150),
            'reply_to' => $this->string(150)->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%email_template}}');
    }
}
