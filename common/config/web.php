<?php
$config = [
    'components' => [
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'linkAssets' => true,
            'appendTimestamp' => YII_ENV_DEV
        ],
        'sendpulse' => [
            'class' => frontend\modules\api\v1\components\SendPulse::className(),
            'userId' => '641d0b1bd32d60042f8b7c58d6495446',
            'secret' => 'de705f79fe9f7da3872298ce5b0ee796',
            'storageType' => 'session'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => '\common\components\log\MyFileTarget',
                    'levels' => ['info'],
                    'categories' => ['apiRequest'],
                    'logFile' => '@common/runtime/logs/API/emails.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                    'logVars' => []
                ],
            ],
        ],
    ],
    'as locale' => [
        'class' => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ]
];
 
if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
}

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.33.1', '172.17.42.1', '172.17.0.1'],
    ];
}


return $config;
