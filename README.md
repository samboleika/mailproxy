# Мануал 
 **Проект - yii2 starter kit + mysql**


 1) Клонируем содержимое все содержимое в корень сайта.

 2) Делаем composer update из корневой папки.

 3) Создаем бд, и правим в конфиге настройку подключения в .env
```sh
'components' => [
        'db' => [
            'dsn' => 'pgsql:host=localhost;dbname= {dbname}',
            'username' =>  '{username}',
            'password' => '{password}', 
        ],
    ]
```

 4) Правим в конфиге настройку подключения к sendpulse параметры {userId} и {secret} в common/config/web.php
```sh
'components' => [
        'sendpulse' => [
            'userId' => '{userId}',
            'secret' => '{secret}',
        ],
```
 
 5) Запускаем в консоли
```
php console/yii app/setup
```

 6) Правим .env
``` 
FRONTEND_URL    = /
BACKEND_URL     = /admin
STORAGE_URL     = /storage/web
```

7) Demo Users

Login: webmaster
Password: webmaster

Login: manager
Password: manager

Login: user
Password: user

8) Наливаем еще чашечку кофейка и чекаем все